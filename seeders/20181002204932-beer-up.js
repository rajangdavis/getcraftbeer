const beers = require("../scrape_extract_data/flat_beer_list.json")
const {sequelize} = require("../models/index.js")

module.exports = {
  up: (queryInterface, Sequelize) => {
    
      // Add altering commands here.
      // Return a promise to correctly handle asynchronicity.
    
      // Example:
      return queryInterface.bulkInsert('beers', beers.map(beer =>{
        return {
          name: beer.name,
          ba_link: beer.link,
          abv: beer.abv,
          rating_counts: beer.rating_counts,
          total_score: beer.total_score,
          brewery_link: beer.brewery_link,
          style_link: beer.style_link
        }
      }), {})
  },

  down: (queryInterface, Sequelize) => {
    
      // Add reverting commands here.
      // Return a promise to correctly handle asynchronicity.

      // Example:
      return queryInterface.bulkDelete('beers', null, {});
  }
};
