const http = require('http');
const express = require('express');
const { localBeerDictionaries, beerNames, reviewInfoFor } = require("./vector_to_string.js")
const env = process.env; 

const app = express();
const server = http.createServer(app);

app.use(express.static(__dirname + '/public'));

app.post('/beersNearMe', async (req, res, next) => {
  try{
    let parsedRequest = JSON.parse(req.headers.body)
    let localBeers = await localBeerDictionaries(parsedRequest)
    res.send(localBeers);
  }catch(err){
    console.log(err)
    res.send(JSON.stringify({err: "There was an error with your request."}));
  }
});  

app.get('/beerNames', async (req, res, next) => {
   try{
    res.sendFile(`${__dirname}/beer_names.json`)
  }catch(err){
    console.log(err)
    res.send(JSON.stringify({err: "There was an error with your request."}));
  }
})


app.post('/reviewInfoFor', async (req, res, next) => {
   try{
    let obj = JSON.parse(req.headers.body)
    console.log(obj)
    let reviewInfo = await reviewInfoFor(obj)
    res.send(reviewInfo)
  }catch(err){
    console.log(err)
    res.send(JSON.stringify({err: "There was an error with your request."}));
  }
})

app.get('/', async (req, res, next) => {
    res.sendFile(__dirname + '/index.html');
})
 
server.listen(process.env.PORT || 3000, () => {
  console.info('Listening on http://localhost:3000');
});