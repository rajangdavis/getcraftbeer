import pandas as pd

from sqlalchemy import create_engine, text as sql

import os
pg_pass = os.environ['DATABASE_URL']
engine = create_engine(pg_pass)

small = pd.read_csv("./scrape_extract_data/small.csv", index_col=0)

print(small.head())

for i,x in enumerate(small.values.tolist()):   
    query = sql("""
    DELETE FROM reviews WHERE id in (SELECT reviews.id
    FROM reviews, beers 
    WHERE beers.id = reviews.beer_id 
    AND review_text = :rt 
    AND review_overall = :overall 
    AND review_aroma = :aroma
    AND review_taste = :taste
    AND review_palate = :palate
    AND review_appearance = :appearance
    AND review_profilename = :rp 
    AND beers.ba_link != :bl ORDER BY reviews.id);
    """)
    
    print(i)
    with engine.begin() as conn:
        test = conn.execute(query, rt=x[0], 
                        rp=x[2], 
                        bl=x[1], 
                        overall=x[3], 
                        aroma=x[4], 
                        taste=x[5], 
                        palate= x[6], 
                        appearance=x[7])