onmessage = async function(e) {
  console.log('Message received from main script');
  let workerResult = await fetch("/beerNames")
  console.log('Posting message back to main script');
  let resultJson = await workerResult.json()
  postMessage(resultJson);
}