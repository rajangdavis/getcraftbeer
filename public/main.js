let beerNames = []
let localPosition = {}
let localBeers = []
let selectedBeers = []

var myWorker = new Worker('worker.js');

const $ = elem=>{ return Array.from(document.querySelectorAll(elem)) } 

const fetchBeerNames = ()=>{
	myWorker.postMessage("beer");
		console.log('Message posted to worker');
		myWorker.onmessage = function(e) {
			beerNames = e.data
			myWorker.terminate();
	}
}
fetchBeerNames()

const reviewInfoFor = ()=>{
	return fetch("/reviewInfoFor",{
		method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
            "body": JSON.stringify(selectedBeers)
        }
    }).then(response => response.json()).catch(err => console.log(err))
}


const getLocation = () =>{
	navigator.geolocation.getCurrentPosition(function(position) {
		localPosition = position
		fetch("/beersNearMe",{
			method: "POST",
	        headers: {
	            "Content-Type": "application/json; charset=utf-8",
	            "body": JSON.stringify({lat: position.coords.latitude, long: position.coords.longitude})
	        },
		}).then(response => response.json()).then(json => localBeers = json).catch(err => console.log(err))
	}, function(err){
		if(err.message == "User denied Geolocation"){
			$(".no-geo")[0].innerHTML = "Without your GPS, I can't provide local recommendations. Thanks for trying out this app!"
		}else{
			$(".no-geo")[0].innerHTML = "Your browser does not support geolocation; please user a browser that supports this functionality"
		}
	})
}

getLocation()

$(".key-up").map(input =>{
	input.addEventListener("keydown",function(e){
		if (e.keyCode === 13)
			e.preventDefault()
	})	
})

$(".key-up").map(function(input,index){
	
	let thisInstance = input	
	input.addEventListener("keyup", async function(e){
		if (e.keyCode === 13)
			e.preventDefault()

		if(thisInstance.value && thisInstance.value.length > 0){
			let newUl = document.createElement("ul")
			let sb = selectedBeers.map(x=> x.id)
			beerNames.filter(x=> x.name.toLowerCase().indexOf(thisInstance.value.toLowerCase()) >-1 )
					 .slice(0,11)
					 .filter(x=> !sb.includes(x.id)).map(bn =>{
				let li = document.createElement("li")
				let text = document.createTextNode(bn.name)
				li.addEventListener("click", function(){
					selectedBeers.push(bn);
					thisInstance.nextElementSibling.innerHTML =""
					thisInstance.nextElementSibling.appendChild(text)
					thisInstance.disabled = true
					if(index < 2){
						$(".key-up")[index + 1].className = "key-up"
					}
					thisInstance.nextElementSibling
					thisInstance.removeEventListener("click",function(){console.log("removed listener")})
					let submitButton  = document.querySelectorAll(".submit")[0]
					submitButton.className = "submit"
				})
				li.appendChild(text)
				newUl.appendChild(li)
			})

			thisInstance.nextElementSibling.innerHTML = "" 
			thisInstance.nextElementSibling.appendChild(newUl)
		}

		if(thisInstance.value == undefined || thisInstance.value.replace(" ","") == ""){
			thisInstance.nextElementSibling.innerHTML = "" 
		}
	})
})

$(".submit")[0].addEventListener("click", async function(){
	if(!this.disabled)
		$(".loader")[0].className = "loader"
		this.disabled = true
		$(".key-up").map(x=>{
			x.disabled = true
		})
		try{
			let reviewInfo = await reviewInfoFor()
			let crossTabulated = cosineSimularities(reviewInfo)
			createRecommendations(crossTabulated)
			this.className = "submit hidden"
			this.disabled = false
			$(".loader")[0].className = "loader hidden"
		}catch(err){
			$(".loader")[0].className = "loader hidden"
			this.disabled = false
			console.log(err)
		}
})


const recommendationElements = (beer, recommendList)=>{
	let wrapper = document.createElement("div");
	wrapper.className = "fadeIn result-info"
	let h2 = document.createElement("h2");
	let beerName = selectedBeers.filter(x=> x.id == beer)[0].name

	let headerText = document.createTextNode(`Recommendations for ${beerName}`)
	h2.appendChild(headerText) 
	wrapper.appendChild(h2)
	let newUl = document.createElement("ul");
	recommendList.slice(0,5).map(x=>{
		let listEl = document.createElement("li")
		let divContainer = document.createElement("div")
		let h3 = document.createElement("h3")
		let smallUl = document.createElement("ul")

		let keysToAvoid = ["review_text_vector","beer_name","brewery_name","beer_id","overall","taste","aroma","appearance","palate","link"]
		// let table = document.createElement("table")
		// let tr = document.createElement("tr")

		Object.keys(x).map(key =>{
			if(!keysToAvoid.includes(key) && x[key] != null){
				let cleanKey = key.replace("_"," ").split("").map(((letter,index) => index ==0 ? letter.toUpperCase() : letter)).join("")
				let span = document.createElement("span")
				let strong = document.createElement("strong")
				let strongText = document.createTextNode(cleanKey)
				strong.appendChild(strongText)
				span.appendChild(strong)
				if (key == "distance"){
					span.appendChild(document.createTextNode(`: ${x[key]} miles`))
				}else{
					span.appendChild(document.createTextNode(`: ${x[key]}`))
				}
				let smallLi = document.createElement("li")
				smallLi.appendChild(span)
				// if(x["taste"] != null){
				// 	let scores = ["overall","taste","aroma","appearance","palate"]
				// 	let th = document.createElement("th")
				// 	tr.appendChild(th)
				// 	table.appendChild(tr)
				// 	let innerTr = document.createElement("tr")
				// 	scores.map(score =>{
				// 		let header = document.createTextNode(score)
				// 		th.appendChild(header)

				// 		let td = document.createElement("td")
				// 		let tdVal = document.createTextNode(x[score])
				// 		td.appendChild(tdVal)
				// 		innerTr.appendChild(td)
				// 		table.appendChild(innerTr)
				// 	})
				// 	smallLi.appendChild(table)
				// }
				smallUl.appendChild(smallLi)
			}
		})
		let link = document.createElement("a")
		let beerName = document.createTextNode(`${x.brewery_name} - ${x.beer_name}`)
		link.appendChild(beerName)
		link.href = x.link
		link.target = "_blank"
		h3.appendChild(link)
		divContainer.appendChild(h3)
		divContainer.appendChild(smallUl)
		listEl.appendChild(divContainer)
		newUl.appendChild(listEl)

	})
	wrapper.appendChild(newUl)
	return wrapper
}

const createRecommendations = (crossTabulated)=>{
	Object.keys(crossTabulated).map((x)=>{
		console.log(`Recommendations for ${x}`)
		let list = recommendationElements(x, crossTabulated[x])
		let br = document.createElement("br")
		$(".results")[0].appendChild(list) 
		$(".results")[0].appendChild(br) 
		$(".beforeResults")[0].scrollIntoView({ 
			behavior: 'smooth',
		});
	})
}

// This transforms the tsVectors from
// Postgres into objects/dictionaries with word counts
const cleanVectors = (vectorsToMap) =>{

	// An array that will store the counts
	let beerDictionaries = []
	// Loop through the array of objects/dicts
	vectorsToMap.filter(x=> x.review_text_vector != null &&  x.review_text_vector != '' ).map(tsv =>{
		let counts = {}
		// split on a text_vector string
		// \'glass\':42 \'goe\':118 \'good\':35,83,98,105,111,113
		// becomes
		// ["'glass':42", "'goe':118", "'good':35,83,98,105,111,113"]
		tsv.review_text_vector.split(" ").map(x => { 
			// ["'glass':42", "'goe':118", "'good':35,83,98,105,111,113"]
			// becomes
			//["'glass':42", "'goe':118", "'good':35,83,98,105,111,113"]
			let v = x.replace(/'/g,"").split(':')
			if(v.length!=2){
				// console.log(tsv)
				// console.log(v)
			}
			//["'glass':42", "'goe':118", "'good':35,83,98,105,111,113"]
			// becomes
			//[["glass", "42"]
			// ["goe", "118"]
			// ["good", "35,83,98,105,111,113"]]

			// Create a key if one does not exist
			if(counts[v[0]] == undefined){
				counts[v[0]] = 0;
			}
			// increment based on the numbers
			counts[v[0]]+= v[1].split(",").length
		})

		beerDictionaries.push(counts)
	})
	return beerDictionaries
}

const cosineObject = (dictionary1, dictionary2)=>{
	// Initialize the dot product
	// and the values of squaring
	// the matrices
    let numerator = 0
    let dena = 0
    let denb = 0

    for (prop in dictionary1){
    	// dot product calculation
    	numerator += dictionary1[prop] * (dictionary2[prop] !=undefined ? dictionary2[prop] : 0)
    	// squared matrix number 1
    	dena += dictionary1[prop] * dictionary1[prop]
    }
    // We don't check against the first dict
    // Because we have all of the words from second dict
    // We would be adding 0's
    for(prop in dictionary2){
    	// squared matrix number 2
        denb += dictionary2[prop] * dictionary2[prop]
    }
    // Cosine Simularity
    return numerator/Math.sqrt(dena * denb)
}

const cosineSimularities = (beersList)=>{

	let results = beersList.concat(localBeers)

	let countDicts = cleanVectors(results)

	let matrix = countDicts.slice(0, beersList.length).map(dict =>{
		let row = []
		countDicts.map(innerDict =>{
			row.push(cosineObject(dict, innerDict))
		})
		return row
	})

	// https://stackoverflow.com/questions/46622486/what-is-the-javascript-equivalent-of-numpy-argsort
	// https://en.wikipedia.org/wiki/Schwartzian_transform
	let cosineSimularityMatches = matrix.map(beerSimilarities =>{

		console.log(results
		  .map((item, index) => [beerSimilarities[index], item])) // add the clickCount to sort by)

		let test =  results
		  .map((item, index) => [beerSimilarities[index], item]) // add the clickCount to sort by
		  .sort(([count1], [count2]) => count2 - count1) // sort by the clickCount data
		  .map(([, item]) => item); // extract the sorted items

		return test
	})

	$(".clear.hidden")[0].className = "clear";

	filteredMatchesObj = {}

	let filteredMatches = cosineSimularityMatches.map( (x,i) =>{
		// let filteredMatch = x
		let filteredMatch = x.filter( y => !beersList.map(bl => bl.beer_id).includes(y.beer_id) )
		filteredMatchesObj[beersList[i].beer_id] = filteredMatch
	})
	return filteredMatchesObj

}

$(".clear")[0].addEventListener("click",function(e){
	selectedBeers = [];
	this.className = "clear hidden";
	let submitButton  = document.querySelectorAll(".submit")[0]
	submitButton.className = "submit hidden"

	$(".key-up").map((el,i) =>{
		el.disabled = false;
		el.value = ""
		if(i> 0){
			el.className = "key-up hidden"
		}
	})
	$(".matches,.results").map(el =>{
		el.innerHTML = ""
	})
})	