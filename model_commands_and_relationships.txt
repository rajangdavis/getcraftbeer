## Production site command for resetting the database:
psql $DATABASE_URL -c "CREATE EXTENSION postgis; set client_encoding to 'UTF8';" && sequelize db:migrate && psql $DATABASE_URL -c "ALTER TABLE reviews ADD COLUMN review_text_vector TSVECTOR;" && sequelize db:seed:all && psql $DATABASE_URL -c "UPDATE beers SET brewery_id = breweries.id FROM breweries WHERE beers.brewery_link = breweries.ba_link;" && psql $DATABASE_URL -c "UPDATE beers SET style_id = styles.id FROM styles WHERE beers.style_link = styles.ba_link;" && psql $DATABASE_URL -c "ALTER TABLE beers DROP COLUMN style_link,DROP COLUMN brewery_link;" && cat ./scrape_extract_data/matched_reviews.csv | psql $DATABASE_URL -c "COPY reviews (beer_id, review_appearance,review_aroma,review_overall,review_palate,review_profilename,review_taste,review_text,review_time) FROM STDIN WITH (FORMAT CSV, HEADER TRUE);" && psql $DATABASE_URL -c "UPDATE reviews SET beer_id = beers.id FROM beers WHERE reviews.beer_id = CAST(replace(substring(beers.ba_link FROM '[0-9]+/$'), '/', '') AS int);" && psql $DATABASE_URL -c "UPDATE reviews SET review_text_vector = to_tsvector(reviews.review_text) WHERE char_length(reviews.review_text) > 13;"




# Bash aliases
alias pg='PGPASSWORD=$PG_PASS psql -U postgres'
alias sequelize='./node_modules/.bin/sequelize'
alias db_reset='sequelize db:drop && sequelize db:create && pg -d craft_beer_development -c "CREATE EXTENSION postgis; set client_encoding to 'UTF8';" && sequelize db:migrate && pg -d craft_beer_development -c "ALTER TABLE reviews ADD COLUMN review_text_vector TSVECTOR;" && sequelize db:seed:all && pg -d craft_beer_development -c "UPDATE beers SET brewery_id = breweries.id FROM breweries WHERE beers.brewery_link = breweries.ba_link;" && pg -d craft_beer_development -c "UPDATE beers SET style_id = styles.id FROM styles WHERE beers.style_link = styles.ba_link;" && pg -d craft_beer_development -c "ALTER TABLE beers DROP COLUMN style_link,DROP COLUMN brewery_link;" && cat ./scrape_extract_data/matched_reviews.csv | pg -d craft_beer_development -c "COPY reviews (beer_id, review_appearance,review_aroma,review_overall,review_palate,review_profilename,review_taste,review_text,review_time) FROM STDIN WITH (FORMAT CSV, HEADER TRUE);" && pg -d craft_beer_development -c "UPDATE reviews SET beer_id = beers.id FROM beers WHERE reviews.beer_id = CAST(replace(substring(beers.ba_link FROM '"'"'[0-9]+/$'"'"'), '"'"'/'"'"', '"'"''"'"') AS int);" && pg -d craft_beer_development -c "UPDATE reviews SET review_text_vector = to_tsvector(reviews.review_text) WHERE char_length(reviews.review_text) > 13;"'





## Postgres specific commands

# local => remote changes
pg -d craft_beer_development # => psql $DATABASE_URL

# Installing PostGIS locally
pg -d craft_beer_development -c "CREATE EXTENSION postgis; set client_encoding to 'UTF8';"

# Feeds the CSV into the DB locally
cat ./scrape_extract_data/matched_reviews.csv | pg -d craft_beer_development -c "COPY reviews (beer_id, review_appearance, review_aroma, review_overall, review_palate, review_profilename, review_taste, review_text, review_time) FROM STDIN WITH (FORMAT CSV, HEADER TRUE);"






## Sequelize specific commands

# Create database
sequelize db:create

# Migrate database
sequelize db:migrate

# Seed database (all of the seeds)
sequelize db:seed:all

# Drop database
sequelize db:drop

# Generating models
# Kind of dumb, but the attributes cannot have spaces between the commas
sequelize model:generate --name Brewery --attributes name:string,address:string,city:string,state:string,website:string,latitude:float,longitude:float,ba_link:string
sequelize model:generate --name Style --attributes abvRange:string,ibuRange:string,glassware:string,description:text
sequelize model:generate --name Beer --attributes name:string,abv:float,rating_counts:integer,total_score:float
sequelize model:generate --name Review --attributes beer_name:string,review_appearance:float,review_aroma:float,review_overall:float,review_palate:float,review_profilename:float,review_taste:float,review_text:text,review_time:int


# Seeding models

sequelize seed:generate --name brewery-up
sequelize seed:generate --name beer-up
sequelize seed:generate --name style-up