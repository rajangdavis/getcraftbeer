'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('reviews', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      beer_id: {
        type: Sequelize.INTEGER
      },
      review_overall: {
        type: Sequelize.FLOAT
      },
      review_taste: {
        type: Sequelize.FLOAT
      },
      review_aroma: {
        type: Sequelize.FLOAT
      },
      review_palate: {
        type: Sequelize.FLOAT
      },
      review_appearance: {
        type: Sequelize.FLOAT
      },
      review_text: {
        type: Sequelize.TEXT
      },
      review_profilename: {
        type: Sequelize.STRING
      },
      review_time: {
        type: Sequelize.INTEGER
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('reviews');
  }
};