const {sequelize, Sequelize} = require("../models/index.js")
const axios = require("axios")
const cheerio = require("cheerio")
const {extractScores} = require("./scrape_beer_html.js")
const DataTypes = Sequelize.DataTypes;

const fetchBeerPage = async (beerReviews, beerObject) =>{
	let beerHtmlPages = beerReviews.map(async function(link,i){
		let beerPage = await axios.get(link)
		let beerPageHtml = cheerio.load(beerPage.data)
		if(i == 0){
			let beerInfo = beerPageHtml("#info_box")
			let infoBoxChildren = beerInfo.children()
			try{

				let availability = infoBoxChildren['22'].prev.data ? infoBoxChildren['22'].prev.data.trim() : infoBoxChildren['20'].prev.data.trim()
				let notes = infoBoxChildren['25'].next.data ? infoBoxChildren['25'].next.data.trim() : infoBoxChildren['24'].next.data.trim()
				const Beers = sequelize.define('beers', {
				    id: {
				    	type: DataTypes.INTEGER, 
				    	primaryKey: true
				    },
					ba_availability: DataTypes.STRING,
					ba_description: DataTypes.TEXT,
				}, {timestamps: false});

				let updateQuery = await Beers.update(
					{
						ba_availability: availability,
						ba_description: notes,
					},
					{
						where: {
							id: beerObject.id
						}
					}
				)
				extractScores(beerPageHtml).map(async (review, index) =>{

					const Reviews = sequelize.define('reviews', {
					    beer_id: DataTypes.INTEGER,
						review_overall: DataTypes.FLOAT,
						review_taste: DataTypes.FLOAT,
						review_aroma: DataTypes.FLOAT,
						review_palate: DataTypes.FLOAT,
						review_appearance: DataTypes.FLOAT,
						review_text: DataTypes.TEXT,
						review_profilename: DataTypes.STRING,
						review_time: DataTypes.INTEGER
					}, {timestamps: false});
					
					let result = await Reviews.create(review)
					let beerIdQuery = "SELECT id FROM beers WHERE ba_link = '"+beerObject.ba_link+"';"
					let beerId = await sequelize.query(beerIdQuery, { type: sequelize.QueryTypes.SELECT})
					let finalUpdate = await Reviews.update({beer_id: beerId[0].id},{where:{id: result.dataValues.id}})
				})
				
			}catch(err){
				console.log(err)
			}
		}

	})

}

const queryBeersByCloseness = `
SELECT 
ST_Distance(  
	ST_GeomFromText('POINT(34.01080683333333 -118.4922238333333)', 4326),
	ST_GeomFromText(ST_AsText(bw.position), 4326)
) AS distance, 
bw.name as bw_name, 
bw.city, bw.state,
b.name
FROM breweries bw, reviews r,
beers b WHERE b.brewery_id = bw.id 
AND b.id = r.beer_id
AND b.rating_counts > 0 
AND b.id NOT IN (      
	SELECT DISTINCT beer_id         
	FROM reviews    
	WHERE beer_id IS NOT NULL 
)
GROUP BY 
bw.position, bw.name, 
bw.city, bw.state, b.name,
b.rating_counts
HAVING COUNT(r.id) = 0
ORDER BY distance, rating_counts DESC;`


const queryResults = async (query)=>{
  try{
    return await sequelize.query(query, { type: sequelize.QueryTypes.SELECT})
  }catch(err){
    console.log(err)
  }
}

// Creates an array of URL's
// that are paginated
const mapPages = (beer) =>{
	let currentPage = beer.ba_link
	let reviewCounts = parseInt(beer.rating_counts)
	let numPages = reviewCounts <= 25 ? 1 : 4
	var pages = []
	for(let i = 0; i < numPages; i++){
		pages.push(`${currentPage}?sort=topr&start=${25 * i}`)
	}
	return pages;
}


const fetchInLoop = async (data, set) =>{
	if (set == undefined){
		set = {
			min: 0,
			max: 4
		}
	}
	
	var promises = data.slice(set.min, set.max).map(async beer =>{
		let beerReviews = mapPages(beer)
		console.log(`Grabbing ${beer.name} (${beer.bw_name}) Info - ${beerReviews.length} pages`)
		return await fetchBeerPage(beerReviews, beer)
	})

	Promise.all(promises).then(function(results) {
		console.log(`Completed set of ${set.min} and ${set.max}`)
	    let newSet = {
			min: set.min + 4,
			max: set.max + 4
		}
		setTimeout(()=>{fetchInLoop(data, newSet)}, 2000)
	}).catch(err =>{
		console.log(err)
		setTimeout(()=>{fetchInLoop(data, newSet)}, 10000)
	})
}


// This runs everything
(async ()=>{
	
	let beersByCloseness = await queryResults(queryBeersByCloseness)

	fetchInLoop(beersByCloseness).then(res =>{
		
	}).catch(err => {
		console.log(err)
	})

})()
