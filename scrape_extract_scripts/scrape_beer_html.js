const cheerio = require("cheerio")

const userRating = `#rating_fullview .user-comment div#rating_fullview_content_2`

module.exports = {
	
	extractScores: (html) =>{

		let extractedDivs = Array.from(html(userRating))
		    .map(x => x.children
		    		   .map(x =>{
		    		   	if(x.type == "text"){
		    		   		x.data = x.data + "(--*--)"; 
		    		   	}else if(x.children){
		    		   		x.children.map(c =>{ 
		    		   			c.data = c.data + "(--*--)"; 	
		    		   		})
		    		   	}
		    		   	return x }))
		    .map(x => cheerio(x).text().split('(--*--)'))
		    .map(x => x.map(e => e.trim())
		    			.filter(e => e.length > 0)
		    			.filter(e => e.indexOf(" characters") == -1)
		    			.filter(e => e != "/5")
		    			.filter(e => e.indexOf("%") == -1)
		    			.filter(e => e.indexOf("rDev") == -1))
	    
		return extractedDivs.map((x,i) => {
			let review = {
				review_text: ""
			}

			x.map((e,j) =>{
				if(j == 0){
					review.review_overall = parseFloat(e)
				}
				else if(j == 1 & e.indexOf("look:") == 0){
					let scores = e.split(" | ").map(s => s.split(":").map(v => v.trim()))
					scores.map(k => {
						if (k[0] == "look"){
							k[0] = "appearance"
						}
						if (k[0] == "smell"){
							k[0] = "aroma"
						}
						if (k[0] == "feel"){
							k[0] = "palate"
						}
						review[`review_${k[0]}`] = parseFloat(k[1])
					})
				}
				else if(j < x.length - 1){
					review.review_text += `${e}\n`
				}else{
					review.review_profilename = e.substr(0,e.indexOf(','))
					let postingDateString = e.replace(e.substr(0,e.indexOf(',')) + ", ","")
					let timeToUnixTime = new Date(postingDateString).getTime() / 1000 
					if(!Number.isNaN(timeToUnixTime)){
						review.review_time = timeToUnixTime
					}
				}
			})
			return review
		})

	}
}
