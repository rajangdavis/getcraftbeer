const axios = require("axios")
const breweries = require("../scrape_extract_data/breweries.json")
const fs = require("fs")
const util = require('util')
const fs_writeFile = util.promisify(fs.writeFile)
const fs_exist = util.promisify(fs.stat)

// Loop through the breweries
// and grab GPS data
const writeToFile = async brewery=>{
	let breweryId = parseInt(brewery.link.split("/")[5])
	let cleanString = `${brewery.address}, ${brewery.city}, ${brewery.state}, ${brewery.zipcode}`;
	let apiUrl = `https://api.opencagedata.com/geocode/v1/json?q=${encodeURI(cleanString)}&key=${process.env.OPEN_CAGE}`.replace("#","%23")
	let cleanFile = `${__dirname}/../scrape_extract_data/brewery_geo_latlong/${breweryId}.json`
	try{
		let fileExists = await fs_exist(cleanFile)
		console.log("File exists")
	}catch(err){
		console.log(`File does not exist: creating for ${brewery.name}`)
		console.log(apiUrl)
		let latLongResponse = await axios.get(apiUrl)
		return await fs_writeFile(cleanFile,JSON.stringify(latLongResponse.data))
	}
}

const fetchInLoop = async (data, set) =>{
	if (set == undefined){
		set = {
			min: 0,
			max: 4
		}
	}
	var promises = data.slice(set.min, set.max).map(async (brewery,i) =>{
		if(brewery.address){
			console.log("Sending request for brewery: " + brewery.name);
			return await writeToFile(brewery)
		}
	})

	Promise.all(promises).then(function(results) {
		console.log(`Completed set of ${set.min} and ${set.max}`)
	    let newSet = {
			min: set.min + 4,
			max: set.max + 4
		}
		setTimeout(()=>{fetchInLoop(data, newSet)}, 1000)
	}).catch(err =>{
		console.log(err)
		fetchInLoop(data, set)
	})
}

fetchInLoop(breweries.reverse()).then(res =>{
	
}).catch(err => {
	console.log(err)
})